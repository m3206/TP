## 

Avant de commencer... commentez l'issue 15 [ici](https://gitlab.com/m3206/TP/issues/15)

Dans tous les cas, vous devez mettre vos scripts sur votre dépôt git. 

## Préliminaires

- Créer un dépôt m3206 sur votre compte gitlab ou github (si ce n'est pas fait).
- Tous les scripts doivent se trouver dans ce dépôt dans un répertoire `scripts`
- ATTENTION: à la fin du TP, une fois que vous avez fait sur votre dépots (`git add`, `git commit`, `git push`),
créez un tag avec la commande: `git tag TP1`, et envoyer ce tag sur votre dépôt avec `git push --tags`

## Exercice 1: Scripts d'initialisation
### Exercice 1.1: 

Ecrire un script qui permet de vérifier si votre ordinateur est connecté à internet. 
```bash
$ check_internet.sh
```

Ce script retourne "0" (avec `exit 0`) si l'ordinateur est connecté, un autre code sinon. La sortie
de ce script doit juste être:
```bash
$ check_internet.sh
[...] Checking internet connection [...]
[...] Internet access OK           [...]
```
ou
```bash
$ check_internet.sh
[...] Checking internet connection [...]
[/!\] Not connected to Internet    [/!\]
[/!\] Please check configuration   [/!\]
```

### Exercice 1.2

Ecrivez un script qui met à jours le système, en supposant que celui-ci utilise le système de paquet de debian/ubuntu.
Vous devez vérifier que vous avez les droits pour effectuer la mise à jour.

```bash
$ update_system.sh
[...] update database [...]
[...] upgrade system  [...]
```
ou
```bash
$ update_system.sh
[/!\] Vous devez être super-utilisateur [/!\]
```

### Exercice 1.3

Ecrivez un script qui vérifie que les outils suivant sont installés: `git, tmux, vim, htop`
```bash
$ check_tools.sh
[...] git: installé       [...]
[...] tmux: installé      [...]
[/!\] vim: pas installé   [/!\] lancer la commande: `apt-get install vim`
[...] htop: installé      [...]
```

### Exercice 1.4
Ecrivez un script qui vérifie que le démon ssh est bien installé et qu'il tourne. Sinon le lancer.
```bash
$ check_ssh.sh
[...] ssh: fonctionne     [...]
```
ou
```bash
$ check_ssh.sh
[...] ssh: est installé                [...]
[/!\] ssh: le service n'est pas lancé  [/!\]
[...] ssh: lancement du service        [/!\] lancer la commande: `/etc/init.d/ssh start`
```

### Exercice 1.5
Mettez les scripts dans un seul scripts.

## Exercice 2: Introspection
### Exercice 2.1

- Ecrire un script `topcomm.sh` permettant de lister le TOP 10 des commandes que vous utilisez le plus.
- Publier votre script sur votre dépôt.
- NOTE: Dans le top 10, le  nombre du top doit être un paramètre de votre script. Pour un top 10, la commande
et le résultat seront les suivant:

```bash
$ ./topcomm.sh 10
 470 ls
 308 cd
 134 _
 114 vim
 102 java
  69 javac
  51 mv
  51 git
  50 cat
  46 mixer
``` 

Pour un top 5:
```bash
$ ./topcomm.sh 5
 470 ls
 308 cd
 134 _
 114 vim
 102 java
``` 