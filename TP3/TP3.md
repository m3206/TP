__Avant de commencer, merci de laisser un commentaire sur l'"issue" numéro 15 `Je me présente !!!` (si ce n'est pas déjà fait).
Si vous ne le faites pas, je ne peux pas corriger vos TPs et si je ne peux pas corriger vos TPs...__

__Mettez une photo sur votre compte gitlab__

Dans tous les cas, vous devez mettre vos scripts sur votre dépôt git. 

## Préliminaires

- Créer un dépôt m3206 sur votre compte gitlab ou github (si ce n'est pas fait).
- Tous les scripts doivent se trouver dans ce dépôt dans un répertoire `scripts`
- ATTENTION: à la fin du TP, une fois que vous avez fait sur votre dépots (`git add`, `git commit`, `git push`),
créez un tag avec la commande: `git tag TP3`, et envoyer ce tag sur votre dépôt avec `git push --tags`


### Exercice 1: Un peu d'organisation

Ecrire un script qui range les fichiers d'un répertoire donné. Pour cela, vous pouvez vous aider de la commande `file --mime-type -b filename`

Un exemple:

```bash
$ classer.sh

>> rangements des fichiers
>> Done!
```

Exemple
Avant:

```bash
Downloads
│   ├── project.docx
│   ├── 21 Guns.mp3
│   ├── Sultans of Swing.mp3
│   ├── report.pdf
│   ├── charts.pdf
│   ├── VacationPic.png
│   ├── CKEditor.zip
│   ├── Cats.jpg
│   └── archive.7z
```

Après:

```bash
Downloads
│   ├── Music
│   │   ├── 21 Guns.mp3
│   │   └── Sultans of Swing.mp3
|   |
│   ├── Documents
│   │   ├── project.docx
│   │   ├── report.pdf
│   │   └── charts.pdf
|   |
│   ├── Archives
│   │   ├── CKEditor.zip
│   │   └── archive.7z
|   |
│   ├── Pictures
│   │   ├── VacationPic.png
│   │   └── Cats.jpg
|   |
│   └── Inclassables
```

On peut se simplifier la tâche mais ce n'est pas très versatile/efficace en faisant une liste
d'extension:

```bash
Music     ['.mp3', '.aac', '.flac', '.ogg', '.wma', '.m4a', '.aiff', '.wav', '.amr'],
Videos    ['.flv', '.ogv', '.avi', '.mp4', '.mpg', '.mpeg', '.3gp', '.mkv', '.ts', '.webm', '.vob', '.wmv'],
Pictures  ['.png', '.jpeg', '.gif', '.jpg', '.bmp', '.svg', '.webp', '.psd', '.tiff'],
Archives  ['.rar', '.zip', '.7z', '.gz', '.bz2', '.tar', '.dmg', '.tgz', '.xz', '.iso', '.cpio'],
Documents ['.txt', '.pdf', '.doc', '.docx','.odf', '.xls', '.xlsv', '.xlsx', '.ppt', '.pptx', '.ppsx', '.odp', '.odt', '.ods', '.md', '.json', '.csv'],
eBooks    ['.mobi', '.epub', '.chm'],
```



### Exercice 2: Un peu de réseau, un truc simple

#### Exercice 2.1

Ecrire un script qui vous donne les informations suivantes:

```bash
$ netinfo.sh
Distribution/OS:                FreeBSD 11.0-RELEASE-p2 amd64
hostname:                       fbsd
uptime:                         2:39
adresse IP:                     10.210.160.167
route par défaut:               10.210.0.1
serveur dns:                    8.8.4.4
service sur le port 22:         ssh                             #(ou ftp, ...)
service sur le port 80:         aucun                           #(ou apache, nginx ...)
```

#### Exercice 2.2

Ecrire un script qui permet de lister toutes les machines conencté au réseau, sur votre sous réseau:

```bash
$ scan_network.sh
The following IP address are responding:
10.210.0.1
10.210.21.168
...
```

#### Exercice 2.3

Ecrire un script en se basant sur le précédent qui test dans un sous réseau donné en paramètre:
- Si un ordinateur répond
- Si il répond, lister tous les ports ouverts sur ces ordinateurs


```bash
$ scan_network_and_ports.sh 192.168.1.0/24
The following IP address are responding:
192.168.1.1
    - open ports are: 22, 80, 25, 8080
192.168.1.10
    - open ports are: 22
...
```