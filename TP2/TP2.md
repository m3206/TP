__Avant de commencer, merci de laisser un commentaire sur [l'"issue" numéro 15 `Je me présente !!!`](https://gitlab.com/m3206/TP/issues/15).
Si vous ne le faites pas, je ne peux pas corriger vos TPs et si je ne peux pas corriger vos TPs...__

__Mettez une photo sur votre compte gitlab__

Dans tous les cas, vous devez mettre vos scripts sur votre dépôt git. 

## Préliminaires

- Créer un dépôt m3206 sur votre compte gitlab ou github (si ce n'est pas fait).
- Tous les scripts doivent se trouver dans ce dépôt dans un répertoire `scripts`
- ATTENTION: à la fin du TP, une fois que vous avez fait sur votre dépots (`git add`, `git commit`, `git push`),
créez un tag avec la commande: `git tag TP2`, et envoyer ce tag sur votre dépôt avec `git push --tags`

### Exercice 1: Echauffement: sauvegarde, et restauration

#### Exercice 1.1
Ecrire un script qui permet de sauvegarder le contenu d'un répertoire donné en paramètre.
Ce script, archive (`tar`) et compresse (`gzip` ou `bz2`) cette archive.
Le nom de l'archive sera de la forme: `AAAA_MM_JJ_hhmm_repertoire.tar.gz`.
Vous devez placer cette archive dans un répertoire `/tmp/backup`. 

```bash
$ save.sh /home/tah
creation de l'archive: 2016_11_15_1242_tah.tar.gz
```

Attention: Créez un fichier dans votre archive qui contient le chemin absolu du
répertoire.


Pour vous aider, il y a les commande `mv, tar, date, basename` etc...


#### Exercice 1.2
Ecrire un script qui permet de restaurer des sauvegardes. Vous devez dans le dossier
`/tmp/backup` précédent trouver la sauvegarde la plus récente (en fonction du nom), et
restaurer seulement le contenu du répertoire (et sous répertoire) donnée en paramètre.

__Si vous trouvez l'exercice complexe, vous pouvez faire une restauration basique,
c'est à dire, décompresser la dernière archive dans un dossier temporaire, et faire un
`mv` au bon endroit après__


```bash
$ restore.sh e/M3206
Restauration du répertoire /home/tah/e/M3206
A partir de de l'archive: 2016_11_15_1242_tah.tar.gz
```

__Exemple complet:__

Supposons que vous avez la structure de répertoire suivante dans `/home/<user>/` :
```bash
photos
│   ├── 2016
│   |    ├── ma_copine.png
│   |    ├── ma_maman.png
│   |    └──  mon_chat.png
│   ├── 2015
│   |    ├── la_plage.png
│   |    └──  l_autre_plage.png
│   ├── 2014
│   |    └── moi_et_mon_chat.png
```
*une petite archive contenant une struture de répertoire similmaire est dispo 
[ici](https://gitlab.com/m3206/TP/raw/master/TP2/files/photos.tar.gz) ou 
`wget https://gitlab.com/m3206/TP/raw/master/TP2/files/photos.tar.gz` il vous 
la décompresser avec la commande `tar xzvf photos.tar.gz`*. 


Vous voulez sauvegarder le dossier `/home/<user>/photos`
```bash
$ save.sh /home/<user>/photos
creation de l'archive: /tmp/backup/2016_11_15_1242_photos.tar.gz
```

Vous voulez restaurer uniquement le dossier `2014` de votre archive.
```bash
$ cd /tmp/backup
$ restore.sh photos/2014
Restauration du répertoire /home/tah/photos/2014
A partir de de l'archive: 2016_11_15_1242_tah.tar.gz
```


Attention: Utilisez le chemin absolu contenu dans le fichier. 

### Exercice 2: Mon historique.
Mon fichier history, que vous pouvez récupérer ici `wget https://gitlab.com/m3206/TP/raw/master/TP2/files/my_history`,
contient mon historique des commandes. Contrairement à ce que vous avez fait, mon historique à
un format différent. 

#### Exercice 2.1
Ecrire un script qui donne le top de mes commandes avec en paramètre le nombre de commande à afficher:

```bash
$ ./topcommprof.sh 10
 470 ls
 308 cd
 134 _
 114 vim
 102 java
  69 javac
  51 mv
  51 git
  50 cat
  46 mixer
```

NOTE: le sha1 de cette sortie est: `9be2664d5ead49078df3d8eeb0d39b5546886035`
pour vérifier votre sha1sum

```bash
$ ./topcommprof.sh 10 > test && sha1sum test
9be2664d5ead49078df3d8eeb0d39b5546886035 test
```


```bash
$ ./topcommprof.sh 4
 470 ls
 308 cd
 134 _
 114 vim
```

#### Exercice 2.2
Dans mon fichier, la longue chaine de chiffre représente le `timestamp unix`. 
Utilisez votre moteur de recherche favoris pour avoir plus de détail sur le sujet.
En bref, le `timestamp` correspond au nombre de seconde écoulées depuis de `1er Janvier 1970`.

[Ici](http://www.commitstrip.com/fr/2012/12/06/les-codeurs-ont-un-incroyable-talent/) pour une petite minute de relaxation.

Ecrivez un script qui permet d'afficher la date à laquelle la première commande donnée 
en paramètre de mon historique a été lancée. Ce script prend aussi en paramètre le nom du fichier.

Par exemple pour la première commande `ls`

```bash
$ ./datefirstcommprof.sh ls my_history
Fri Nov 11 00:13:52 RET 2016
```

#### Exercice 2.3
Copier votre script `datefirstcommprof.sh` en `datecommprof.sh`. Pour qu'il prenne aussi en paramètre 
`first` ou `last` et affiche la première fois (comme dans le script précédent) ou la dernière fois que la
commande a été lancée.

```bash
$ ./datecommprof.sh first ls my_history
Fri Nov 11 00:13:52 RET 2016
```

```bash
$ ./datecommprof.sh last ls my_history
Fri Nov 17 20:10:12 RET 2016
```

#### Exercice 2.4
Copier votre script `datecommprof.sh` en  `datecomm.sh`, et ecrivez des messages indiquant
à l'utilisateur l'utilisation du script si il manque des paramètres, si les paramètres sont faux,
si la commande n'a jamais été lancée, si le fichier n'existe pas etc...

```bash
$ ./datecomm.sh lastza ls my_history
First argument should be first or last.
usage: ./datecomm.sh [first|last] command file
```
```bash
$ ./datecomm.sh last ls my_histor
Le fichier my_histoir n'existe pas.
usage: ./datecomm.sh [first|last] command file
```

```bash
$ ./datecomm.sh last ls 
The command takes 3 arguments.
usage: ./datecomm.sh [first|last] command file
```

```bash
$ ./datecomm.sh last mplayer my_history
The history does not contain mplayer command
```

### Exercice 3: Relaxation: Jeu

#### Intro

Ecrire un petit jeu. Vous devez écrire un script qui donne des instructions à l'utilisateur
qui lance le script et qui vérifie que l'utilisateur a bien suivi ces instructions. 

Par exemple. Créez un fichier `jeu.sh`. Ce script que vous devrait modifier contiendra dans
un premier temps:

```bash 
$ cat jeu.sh
echo "Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh"
```

Du coup si un utilisateur execute le script il verra:
```bash 
$ ./jeu.sh
Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh
```

Maintenant supposons que l'utilisateur a bien suivi vos instructions (contenu dans le script).
Vous devez modifier `./monjeu.sh` pour vérifier que l'utilisateur à bien fait ces modifications.
Et donc qu'il peut continuer le jeu.

Est ce que je suis clair ? 

#### Ce qu'il y a a faire

Par exemple votre script doit demander à l'utilisateur et vérifier:
- Demander à l'utilsateur de changer le nom du script en `monjeu.sh` 
et de l'executer avec la commande `./monjeu.sh`. Votre script doit vérifier qu'il l'a bien 
fait avant de passer à l'étape suivante.
- Demander à l'utilisateur, dans le répertoire actuel, de créer un fichier `test`. 
Votre script doit vérifier qu'il l'a bien fait avant de passer à l'étape suivante.
- Demander à l'utilsateur de changer les droits sur le fichier `test` en `rw-r-----`. 
Votre script doit vérifier qu'il l'a bien fait avant de passer à l'étape suivante.
- Demander à l'utilisateur de mettre vim comme editeur par défaut (variable d'environnement). 
Votre script doit vérifer qu'il l'a bien fait.
- Demander à l'utilsateur de créer un fichier `compteur` contenant les nombres de 1 à 1000 
(un nombre par ligne). Votre script doit vérifer qu'il l'a bien fait.
- Demander à l'utilsateur de mettre dans un fichier `nbligne`, le nombre de ligne de mon 
historique (exercice précédent). 
- Demander à l'utilsateur de mettre dans un fichier `nbls`, le nombre de fois dans mon
historique ou j'ai l'ancée la commande `ls`. 
- Demander à l'utilsateur de mettre dans un fichier `nbcommande`, le nombre de commande 
de mon historique (exercice précédent) lancée entre le 12 Nov et le 15 Nov. 
- EN BONUS: ... Demander des choses complexes liées au `shell` et à `*NIX`... plein de 
choses complexes... Je le donnerai en examen aux étudiants de première année.


__Pour ce script vous devez fournir le script monjeu.sh mais aussi les commandes ou les
scripts que l'utilisateur est censé faire pour continuer à avancer dans le jeux__

__Evidemment, le mieux est de trouver un moyen pour eviter à l'utilsateur de tricher et regardant
directement dans le script__

Vous pouvez récupérer le début du script ici: `wget https://gitlab.com/m3206/TP/raw/master/TP2/files/jeu.sh`

#### Début de votre script
```bash
#!/bin/bash

if [ "$0" == "./monjeu.sh" ]; then
    echo "Le script est nommé et lancé correctement"
else
    echo "Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh"
    exit 1
done


....
```